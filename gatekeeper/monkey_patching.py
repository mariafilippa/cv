from push_notifications.models import WebPushDevice
from push_notifications.webpush import WebPushError

#monkey patch to catch exceptions
def send_message(self, message, **kwargs):
    from push_notifications.webpush import webpush_send_message

    try:
        return webpush_send_message(
            uri=self.registration_id, message=message, browser=self.browser,
            auth=self.auth, p256dh=self.p256dh,
            application_id=self.application_id, **kwargs)
    except WebPushError as e:
        print(e.message)
        if '410 NotRegistered' in e.message or '410 Gone' in e.message:
            self.delete()
        return {'results': [{'error': e.message}]}

WebPushDevice.send_message = send_message


#add message property to WebPushError
def __init__(self, message):
    self.message = message

WebPushError.__init__ = __init__
