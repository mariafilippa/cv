import json

from django.conf import settings
from django.core.mail import send_mail
from django.db import models
from django.db.models import (Case, Count, F, Q, Sum, Value, When,
                              prefetch_related_objects)
from django.template.loader import get_template
from django.urls import reverse
# from django.contrib.sites.models import Site
from django.utils import timezone
from gatekeeper.monkey_patching import WebPushDevice

KEEPER_LIMIT = 2


class PlanQuerySet(models.QuerySet):
    def count_comments(self):
        return self.annotate(
            count_comments=Count(
                'social_comment',
                filter = Q(social_comment__display=True),
                distinct=True,
            )
        )

    def count_keeper(self):
        return self.annotate(
            keeper_num=Count(
                'plan_krs',
                filter=Q(plan_krs__status='ok'),
                distinct=True,
            )
        )


class PlanManager(models.Manager):
    def get_queryset(self):
        return PlanQuerySet(self.model, using=self._db).select_related(
            'user__profile',
        )

    def count_comments(self):
        return self.get_queryset().count_comments()

    def count_keeper(self):
        return self.get_queryset().count_keeper()


class Plan(models.Model):
    objects = PlanManager()

    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='plan')
    content = models.TextField('創作計畫', help_text='建議格式: 1.自介 2.今年創作目標 3.期待守門方式')
    create_at = models.DateTimeField(auto_now_add=True)
    edit_at = models.DateTimeField(auto_now=True)
    year = models.PositiveIntegerField(default=2018, editable=False)

    class Meta:
        verbose_name = 'Plan'
        verbose_name_plural = 'Plans'
        unique_together = (
            ('user', 'year'),
        )

    def save(self, * args, ** kwargs):
        if not self.pk:
            self.year = timezone.now().year
        super().save(*args, **kwargs)

    def __str__(self):
        return '{0.user.profile.displayname} {0.year} 年度寫作計畫'.format(self)

    def get_gatekeepers(self):
        return self.plan_krs.filter(
            status__in=['ok', 'hi']
        ).select_related(
            'keeper__profile',
            'creator__profile',
        )

    def mailplan(self, maillist, created=True, debug_mode=False):
        action = '建立' if created else '修改'
        subject = '[想像朋友] {0.user.profile.displayname}{1}了創作計畫'.format(self, action)
        html_message = get_template('email/plan-notification.html').render({
            'plan': self,
            'SITE_URL': settings.SITE_URL,
        })
        maillist = [settings.EMAIL_HOST_USER] if debug_mode else maillist  # 測試的時候寄給自己就好
        return send_mail(subject,
                         '',
                         settings.EMAIL_HOST_USER,
                         maillist,
                         html_message=html_message,
                         fail_silently=False)

    def notifyplan(self, notifylist, created=True):
        action = '建立' if created else '修改'
        title = '[想像朋友]'
        body = '{0.user.profile.displayname}{1}了創作計畫'.format(self, action)
        tag = 'if-plan'
        url = self.get_absolute_url()
        message = {
            'title': title,
            'body': body,
            'tag': tag,
            'url': url
        }
        return notifylist.send_message(json.dumps(message))

    def have_sent(self, me):
        return self.plan_krs.filter(Q(keeper=me) | Q(stump=me)).filter(status__in=['ok', 'hi']).exists()

    # def keeper_num(self):
    #     return self.plan_krs.filter(status__in=['ok']).count()

    def get_absolute_url(self):
        return reverse('plan-detail', kwargs={'pk': self.pk})

    def keeper_limit(self):
        return self.plan_krs.filter(status='ok').count() >= KEEPER_LIMIT


class KeeperRequestQuerySet(models.QuerySet):

    def exclude_gg(self):
        return self.exclude(status='gg')

    def get_creator_type(self):
        return self.annotate(
            creator_type=Case(
                When(creator_id=F('keeper_id'), then=Value('keeper')),
                default=Value('stump'),
                output_field=models.CharField(),
            )
        )

    def check_user_is_receiver(self, user):
        return self.annotate(
            user_is_receiver=Case(
                When(receiver_id=user.id, then=Value(True)),
                default=Value(False),
                output_field=models.BooleanField(),
            )
        )


class KeeperRequestManager(models.Manager):
    def get_queryset(self):
        ''' 減少 subquery 的數量 '''
        return KeeperRequestQuerySet(self.model, using=self._db).select_related(
            'keeper__profile',
            'stump__profile',
            'creator__profile',
            'receiver__profile',
            'plan__user__profile',
        )

    def exclude_gg(self):
        return self.get_queryset().exclude_gg()

    def get_creator_type(self):
        return self.get_queryset().get_creator_type()

    def check_user_is_receiver(self, user):
        return self.get_queryset().check_user_is_receiver(user)


class KeeperRequest(models.Model):
    objects = KeeperRequestManager()

    keeper = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='keeper_krs')
    stump = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='stump_krs')
    plan = models.ForeignKey('gatekeeper.Plan', on_delete=models.CASCADE, related_name='plan_krs')
    creator = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='creator_krs')
    receiver = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='receiver_krs')
    STATUS_CHOICES = (
        ('hi', '遞交'),
        ('ok', '答應'),
        ('no', '拒絕'),
        ('gg', '取消'),
        ('-f', '終止')
    )
    status = models.CharField('狀態', max_length=2, choices=STATUS_CHOICES)
    create_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        index_together = (
            ('plan', 'status'),
        )

    @property
    def tokeeper(self):
        return self.receiver == self.keeper

    def creator_name(self):
        return '{0.profile.displayname}'.format(self.creator)

    def receiver_name(self):
        return '{0.profile.displayname}'.format(self.receiver)

    def gen_message(self):
        if self.status == 'hi' and self.tokeeper:
            message = '{0.creator.profile.displayname}邀請您擔任守門人'.format(self)
        elif self.status == 'hi' and not self.tokeeper:
            message = '{0.creator.profile.displayname}申請成為您的守門人'.format(self)
        elif self.status == 'ok' and self.tokeeper:
            message = '{0.receiver.profile.displayname}答應擔任守門人'.format(self)
        elif self.status == 'ok' and not self.tokeeper:
            message = '{0.receiver.profile.displayname}答應您成為守門人'.format(self)
        elif self.status == 'no' and self.tokeeper:
            message = '{0.receiver.profile.displayname}婉拒擔任守門人'.format(self)
        elif self.status == 'no' and not self.tokeeper:
            message = '{0.receiver.profile.displayname}婉拒您成為守門人'.format(self)
        elif self.status == 'gg' and self.tokeeper:
            message = '{0.creator.profile.displayname}取消守門人邀請'.format(self)
        elif self.status == 'gg' and not self.tokeeper:
            message = '{0.creator.profile.displayname}取消守門人申請'.format(self)
        elif self.status == '-f':
            message = '{0.keeper.profile.displayname}與{0.stump.profile.displayname}的守門關係已終止'.format(self)
        return message

    def gen_actions(self):
        if self.status == 'hi':
            return [
                ('keeper_request-accept', '接受邀請'),
                ('keeper_request-refuse', '拒絕邀請'),
            ]
        else:
            return []

    def mailstatus(self, debug_mode=False):
        message = self.gen_message()
        subject = '[想像朋友] {0}'.format(message)

        if self.status in ['hi', 'gg']:
            email = [self.receiver.email]
        elif self.status in ['ok', 'no']:
            email = [self.creator.email]
        else:
            email = [self.receiver.email, self.creator.email]

        if debug_mode:
            email = [settings.EMAIL_HOST_USER]  # 測試的時候寄給自己就好


        html_message = get_template('email/request-notification.html').render({
            'keeper_request': self,
            'message': message,
            'SITE_URL': settings.SITE_URL,
        })

        return send_mail(subject,
                         '',
                         settings.EMAIL_HOST_USER,
                         email,
                         html_message=html_message,
                         fail_silently=False)

    def notifystatus(self, debug_mode=False):
        if self.status in ['hi', 'gg']:
            notifylist = WebPushDevice.objects.filter(user=self.receiver)
        elif self.status in ['ok', 'no']:
            notifylist = WebPushDevice.objects.filter(user=self.creator)
        else:
            notifylist = WebPushDevice.objects.filter(user__in=[self.receiver, self.creator])

        if debug_mode:
            notifylist = WebPushDevice.objects.filter(name="BoJack", browser="CHROME")  # 測試的時候寄給自己就好

        title = '[想像朋友]'
        body = self.gen_message()
        tag = 'if-request'
        url = reverse('keeper_request-list')
        message = {
            'title': title,
            'body': body,
            'tag': tag,
            'url': url
        }
        return notifylist.send_message(json.dumps(message))

class CommentQuerySet(models.QuerySet):
    def check_user_is_author(self, user):
        return self.annotate(
            user_is_author=Case(
                When(user_id=user.id, then=Value(True)),
                default=Value(False),
                output_field=models.BooleanField(),
            )
        )

    def exclude_hidden(self):
        return self.exclude(display=False)


class CommentManager(models.Manager):
    def get_queryset(self):
        return CommentQuerySet(self.model, using=self._db).select_related('user__profile')

    def check_user_is_author(self, user):
        return self.get_queryset().check_user_is_author(user)

    def exclude_hidden(self):
        return self.get_queryset().exclude_hidden()


class Comment(models.Model):
    objects = CommentManager()

    message = models.TextField('Comment', help_text='Leave your comment here')
    create_at = models.DateTimeField(auto_now_add=True)
    display = models.BooleanField(default=True)

    class Meta:
        abstract = True

    @property
    def user(self):
        raise NotImplementedError

    @property
    def post(self):
        raise NotImplementedError

    def mail_message(self, debug_mode=False):
        subject = '[想像朋友] {0.user.profile.displayname}留言在「{0.post}」'.format(self)
        receiver = self.get_mail_receiver()

        if debug_mode:
            email = settings.EMAIL_HOST_USER # 測試的時候寄給自己就好
        else:
            email = receiver.email

        if receiver.pk == self.user.pk:  # dont mail himself
            return False

        if not receiver.profile.subscribe_comments_mail:
            return False

        html_message = get_template('email/comment-notification.html').render({
            'comment': self,
            'post': self.post,
            'SITE_URL': settings.SITE_URL,
        })

        return send_mail(subject,
                         '',
                         settings.EMAIL_HOST_USER,
                         [email],
                         html_message=html_message,
                         fail_silently=False)

    def notify_message(self, debug_mode=False):
        receiver = self.get_mail_receiver()

        if receiver.pk == self.user.pk:  # dont mail himself
            return False

        if not receiver.profile.subscribe_comments_mail:
            return False

        if debug_mode:
            notifylist = WebPushDevice.objects.filter(name="BoJack") # 測試的時候寄給自己就好
        else:
            notifylist = WebPushDevice.objects.filter(user=receiver)


        title = '[想像朋友] {0.user.profile.displayname}留言在「{0.post}」'.format(self)
        body = self.message
        tag = 'if-request'
        url = self.get_absolute_url()
        message = {
            'title': title,
            'body': body,
            'tag': tag,
            'url': url
        }
        return notifylist.send_message(json.dumps(message))

    def get_mail_receiver(self):
        raise NotImplementedError

    def get_absolute_url(self):
        raise NotImplementedError


class PlanComment(Comment):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='plan_comment')
    post = models.ForeignKey('gatekeeper.Plan', on_delete=models.CASCADE, related_name='social_comment')

    def get_mail_receiver(self):
        return self.post.user

    def get_absolute_url(self):
        return reverse('plan-detail', kwargs={'pk': self.post.pk})

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'
        index_together = (
            ('post', 'display'),
        )


class GatekeeperComment(Comment):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='profile_comment')
    post = models.ForeignKey('ifsite.Profile', on_delete=models.CASCADE, related_name='social_comment')

    def get_mail_receiver(self):
        return self.post.user

    def get_absolute_url(self):
        return reverse('gatekeeper-detail', kwargs={'pk': self.post.pk})

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'
