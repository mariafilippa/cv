# Generated by Django 2.0.3 on 2018-04-09 17:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gatekeeper', '0003_plancomment_profilecomment'),
    ]

    operations = [
        migrations.AddField(
            model_name='gatekeepercomment',
            name='display',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='plancomment',
            name='display',
            field=models.BooleanField(default=True),
        ),
    ]
