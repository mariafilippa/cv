# Generated by Django 2.0.3 on 2018-04-10 17:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gatekeeper', '0004_auto_20180410_0117'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='keeperrequest',
            index_together={('plan', 'status')},
        ),
    ]
