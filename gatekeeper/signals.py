from datetime import timedelta

from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from gatekeeper.models import Plan, KeeperRequest, PlanComment, GatekeeperComment
from gatekeeper.monkey_patching import WebPushDevice
from ifsite.models import Profile


@receiver(post_save, sender=Plan)
def mailplan_when_plan_save(sender, instance, created, **kwargs):
    plan = instance
    if created:
        # user celery beat task send new plans email:
        return False
        # send to all gatekeeper and newcomes
        # maillist = list(Profile.objects.new_plan_mail_targets(13).get_email())  # 應該隨時間改動 grade 參數？
    else:
        # only send to the plan's gatekeeper
        maillist = []
        for keeper_request in plan.get_gatekeepers():
            maillist.append(keeper_request.keeper.email)
    maillist = list(set(filter(bool, maillist)))
    plan.mailplan(maillist, created=created, debug_mode=settings.DEBUG)


@receiver(post_save, sender=Plan)
def notifyplan_when_plan_save(sender, instance, created, **kwargs):
    plan = instance
    if created:
        users = Profile.objects.new_plan_mail_targets(13).values_list('user', flat=True)
        notifylist = WebPushDevice.objects.filter(user__in=users)
    else:
        keepers = plan.get_gatekeepers().values_list('keeper', flat=True)
        notifylist = WebPushDevice.objects.filter(user__in=keepers)
    # if settings.DEBUG:
    #     notifylist = WebPushDevice.objects.filter(name="BoJack")
    #     print(notifylist)
    plan.notifyplan(notifylist, created=created)


@receiver(post_save, sender=KeeperRequest)
def mailstatus_when_request_save(sender, instance, created, **kwargs):
    keeper_request = instance
    keeper_request.mailstatus(debug_mode=settings.DEBUG)


@receiver(post_save, sender=KeeperRequest)
def notifystatus_when_request_save(sender, instance, created, **kwargs):
    keeper_request = instance
    keeper_request.notifystatus()


@receiver(post_save, sender=PlanComment)
def mail_plan_comment(sender, instance, created, **kwargs):
    comment = instance
    if comment.display and created:
        comment.mail_message(debug_mode=settings.DEBUG)


@receiver(post_save, sender=PlanComment)
def notify_plan_comment(sender, instance, created, **kwargs):
    comment = instance
    if comment.display and created:
        comment.notify_message()


@receiver(post_save, sender=GatekeeperComment)
def mail_gatekeeper_comment(sender, instance, created, ** kwargs):
    comment = instance
    if comment.display and created:
        comment.mail_message(debug_mode=settings.DEBUG)


@receiver(post_save, sender=GatekeeperComment)
def notify_gatekeeper_comment(sender, instance, created, **kwargs):
    comment = instance
    if comment.display and created:
        comment.notify_message()
