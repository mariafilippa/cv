import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import get_template
from django.utils import timezone
from django.views.generic import DetailView, View
from django.views.generic.edit import CreateView, FormView, UpdateView
from django.views.generic.list import ListView

from gatekeeper import signals
from gatekeeper.forms import PlanForm
from gatekeeper.models import (GatekeeperComment, KeeperRequest, Plan,
                               PlanComment, KEEPER_LIMIT)
from gatekeeper.monkey_patching import WebPushDevice
from ifsite.forms import ProfileForm
from ifsite.models import Profile

from urllib import parse


class ForceProfileMixin:
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if not hasattr(request.user, 'profile'):
                messages.info(request, '請先填寫個人資料，才能使用此功能 :)')
                return redirect('profile')
        return super().dispatch(request, *args, **kwargs)


class KeepGetQueryListView(ListView):
    def copy_get_query_parameters(self):
        get_copy = self.request.GET.copy()
        get_copy.pop('page', True)
        parameters = get_copy.urlencode()
        return parameters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['parameters'] = self.copy_get_query_parameters()
        return context


class ProfileView(LoginRequiredMixin, FormView):
    template_name = 'profile.html'
    form_class = ProfileForm
    success_url = '/'

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        profile, _ = Profile.objects.get_or_create(user=self.request.user, defaults={
            'grade': 13,
        })
        form_kwargs['instance'] = profile
        return form_kwargs

    def form_valid(self, form):
        form.instance.save()
        return super().form_valid(form)


class PlanCreateView(LoginRequiredMixin, ForceProfileMixin, CreateView):
    template_name = 'plan-form.html'
    success_url = '/plan/'
    model = Plan
    fields = ['content']

    def form_valid(self, form):
        form.instance.user = self.request.user
        try:
            form.instance.save()
        except IntegrityError:
            years = ','.join([
                str(year)
                for year
                in self.request.user.plan.all().values_list('year', flat=True)
            ])
            display_text = 'IntegrityError: 每個使用者一年只能有一筆寫作計畫喔。您現在已有 {0} 年度的寫作計畫。'.format(years)
            messages.add_message(self.request, messages.INFO, display_text)
            return redirect('plan-list')

        return super().form_valid(form)


class PlanUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'plan-form.html'
    success_url = '/plan/'
    model = Plan
    fields = ['content']

    def get_object(self):
        ''' only author can update this object '''
        plan = super().get_object()
        if plan.user != self.request.user:
            raise PermissionDenied
        return plan


class PlanListView(LoginRequiredMixin, KeepGetQueryListView):
    template_name = 'plan-list.html'
    model = Plan
    paginate_by = 10

    def store_list_view_query(self):
        self.request.session['plan-list-query'] = self.request.GET.urlencode()

    def get_queryset(self):
        queryset = super().get_queryset().count_comments().count_keeper().order_by('-pk')

        search = self.request.GET.get('search')
        if search:
            queryset = queryset.filter(content__contains=search)

        userfilter = self.request.GET.get('userfilter')
        if userfilter:
            queryset = queryset.filter(
                Q(user__first_name__contains=userfilter)
                | Q(user__last_name__contains=userfilter)
                | Q(user__profile__penname__contains=userfilter)
                | Q(user__profile__area__contains=userfilter))

        hidefull = self.request.GET.get('hidefull')
        if hidefull:
            queryset = queryset.filter(keeper_num__lt=KEEPER_LIMIT)

        self.store_list_view_query()

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data( ** kwargs)
        year = timezone.now().year
        my_plan_id = self.request.user.plan.filter(year=year).values_list('pk', flat=True).last()
        if my_plan_id:
            context['my_plan_id'] = my_plan_id

        context['parameters'] = self.copy_get_query_parameters()

        return context


class PlanDetailView(LoginRequiredMixin, DetailView):
    model = Plan
    template_name = "plan-detail.html"

    def load_list_view_query_params(self):
        list_view_params = self.request.session.get('plan-list-query', '')
        if list_view_params:
            return '?' + list_view_params
        else:
            return ''

    def get_context_data(self, **kwargs):
        user = self.request.user
        context = super().get_context_data( ** kwargs)
        plan = self.object
        context['have_sent'] = plan.have_sent(user)
        context['comments'] = plan.social_comment.exclude_hidden().order_by('create_at').check_user_is_author(user)
        context['parameters'] = self.load_list_view_query_params()
        return context


class GatekeeperListView(LoginRequiredMixin, KeepGetQueryListView):
    template_name = 'gatekeeper-list.html'
    model = Profile
    paginate_by = 15

    def get_queryset(self):
        return super().get_queryset().count_stump().filter(is_gatekeeper=True)


class GatekeeperDetailView(LoginRequiredMixin, DetailView):
    model = Profile
    template_name = "gatekeeper-detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data( ** kwargs)
        stump_id = self.request.user.id
        keeper_id = self.get_object().user_id
        context['have_sent'] = KeeperRequest.objects.filter(stump_id=stump_id, keeper_id=keeper_id).exists()
        context['comments'] = self.get_object().social_comment.exclude_hidden().order_by('create_at').check_user_is_author(self.request.user)
        return context


class ProfileDetailView(LoginRequiredMixin, DetailView):
    model = Profile
    template_name = "profile-detail.html"

    def get(self, request, pk):
        ''' auto redirect to gatekeeper-detail if user is gatekeeper '''
        if self.get_object().is_gatekeeper:
            return redirect('gatekeeper-detail', pk)
        return super().get(request, pk)


class KeeperRequestListView(LoginRequiredMixin, KeepGetQueryListView):
    template_name = 'keeper_request-list.html'
    model = KeeperRequest
    paginate_by = 20

    def get_queryset(self):
        user = self.request.user
        return super().get_queryset().exclude_gg().filter(
            Q(creator=user) | Q(receiver=user)
        ).order_by('-pk').get_creator_type().check_user_is_receiver(user)


class KeeperRequestDetailView(LoginRequiredMixin, DetailView):
    model = KeeperRequest
    template_name = "keeper_request-detail.html"


class UserCantBeKeeper(Exception): pass

class StumpHasNoPlan(Exception): pass

class CreateKeeperRequestBase(LoginRequiredMixin, ForceProfileMixin, View):
    def get(self, request, pk):
        try:
            keeper, stump, creator, receiver, plan=self.make_keeper_request(pk)
        except UserCantBeKeeper:
            messages.info(self.request, '請先填寫自我介紹，並勾選「擔任守門人」，才能擔任守門人 :)')
            return redirect('profile')
        except StumpHasNoPlan:
            messages.info(self.request, '請先撰寫寫作計畫，才能邀請守門人 :)')
            return redirect('plan-create')
        else:
            keeper_request = KeeperRequest(
                keeper=keeper,
                stump=stump,
                plan=plan,
                creator=creator,
                receiver=receiver,
                status='hi'
            )
            keeper_request.save()
        return redirect('keeper_request-list')


class StumpCreateKeeperRequestView(CreateKeeperRequestBase):
    def make_keeper_request(self, profile_id):
        profile = get_object_or_404(Profile, pk=profile_id)
        keeper = profile.user
        stump = self.request.user
        creator = stump
        receiver = keeper
        plan = Plan.objects.filter(user=stump).last()
        if not plan:
            raise StumpHasNoPlan
        return keeper, stump, creator, receiver, plan


class KeeperCreateKeeperRequestView(CreateKeeperRequestBase):
    def make_keeper_request(self, plan_id):
        plan = get_object_or_404(Plan, pk=plan_id)
        if not self.request.user.profile.can_be_a_keeper():
            raise UserCantBeKeeper
        keeper = self.request.user
        stump = plan.user
        creator = keeper
        receiver = stump
        return keeper, stump, creator, receiver, plan


class UpdateKeeperRequestView(LoginRequiredMixin, ForceProfileMixin, View):
    update_to_status = None  # 'ok' or 'no' or 'gg'

    def get_keeper_request(self, pk):
        return get_object_or_404(KeeperRequest, pk=pk)

    def permission_condition(self, keeper_request):
        if self.update_to_status in ['ok', 'no']:
            return keeper_request.receiver == self.request.user
        elif self.update_to_status == 'gg':
            return keeper_request.creator == self.request.user or keeper_request.plan.keeper_limit()
        elif self.update_to_status == '-f':
            return keeper_request.status == 'ok'
        else:
            raise Exception('update_to_status "%s" condition is not defined' % self.update_to_status)

    def get(self, request, pk):
        keeper_request = self.get_keeper_request(pk)
        if self.permission_condition(keeper_request):
            keeper_request.status = self.update_to_status
            keeper_request.save()
        else:
            raise PermissionDenied
        return redirect('keeper_request-list')


class AcceptKeeperRequestView(UpdateKeeperRequestView):
    update_to_status = 'ok'

    def get(self, request, pk):
        keeper_request = self.get_keeper_request(pk)
        if keeper_request.plan.keeper_limit():
            messages.error(self.request, '該計畫守門人已達上限，撤除請求。')
            return redirect('keeper_request-cancel', pk)
        else:
            return super().get(request, pk)


class RefuseKeeperRequestView(UpdateKeeperRequestView):
    update_to_status = 'no'


class CancelKeeperRequestView(UpdateKeeperRequestView):
    update_to_status = 'gg'


class TerminateKeeperRequestView(UpdateKeeperRequestView):
    update_to_status = '-f'


class CreateCommentBase(LoginRequiredMixin, ForceProfileMixin, View):
    def post(self, request, pk):
        user = request.user
        message = request.POST.get('message')
        post = self.get_article(pk)
        return self.save_comment(user, post, message, pk)

    def get_article(self, pk):
        raise NotImplementedError

    def save_comment(self, user, post, message, pk):
        raise NotImplementedError


class CreatePlanCommentView(CreateCommentBase):
    def get_article(self, pk):
        return Plan.objects.get(id=pk)

    def save_comment(self, user, post, message, pk):
        comment = PlanComment(
            user=user,
            post=post,
            message=message
        )
        comment.save()
        return redirect('plan-detail', pk=pk)


class CreateGatekeeperCommentView(CreateCommentBase):
    def get_article(self, pk):
        return Profile.objects.get(id=pk)

    def save_comment(self, user, post, message, pk):
        comment = GatekeeperComment(
            user = user,
            post = post,
            message = message
        )
        comment.save()
        return redirect('gatekeeper-detail', pk=pk)


class DeleteCommentBase(LoginRequiredMixin, View):
    def get(self, request, pk, post_pk):
        comment = self.get_comment(pk)
        if self.permission_condition(comment):
            comment.display = False
            comment.save()
        else:
            raise PermissionDenied

    def permission_condition(self, comment):
        return self.request.user == comment.user

    def get_comment(self, pk):
        raise NotImplementedError
        

class DeletePlanCommentView(DeleteCommentBase):
    def get_comment(self, pk):
        return get_object_or_404(PlanComment, pk=pk)

    def get(self, request, pk, post_pk):
        super().get(request, pk, post_pk)
        return redirect('plan-detail', pk=post_pk)


class DeleteGatekeeperCommentView(DeleteCommentBase):
    def get_comment(self, pk):
        return get_object_or_404(GatekeeperComment, pk=pk)

    def get(self, request, pk, post_pk):
        super().get(request, pk, post_pk)
        return redirect('gatekeeper-detail', pk=post_pk)


class PlanUnsubscribeEmailView(LoginRequiredMixin, ForceProfileMixin, View):
    def get(self, request):
        profile = request.user.profile
        profile.subscribe_plan_mail = False
        profile.save()
        return redirect('profile')


class ServiceWorkerView(View):
    def get(self, request):
        template = get_template('service-worker.js')
        html = template.render()
        return HttpResponse(html, content_type='application/x-javascript')


class SubscribeNotificationsView(LoginRequiredMixin, View):
    def post(self, request):
        data = json.loads(request.body)
        device = WebPushDevice(
            name=request.user.profile.displayname,
            user=request.user,
            registration_id=data['registration_id'],
            p256dh=data['p256dh'],
            auth=data['auth'],
            browser=data['browser']['name'].upper(),
        )
        device.save()
        return JsonResponse({'data':{'success': 1}})
