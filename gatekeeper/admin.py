import csv

from django.contrib import admin
from django.http import HttpResponse
from gatekeeper.models import KeeperRequest, Plan
from ifsite.models import Profile

# from gatekeeper.forms import PlanForm


@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    inlines = [RequestInline]


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    actions = [export_as_csv_action("CSV Export")]
