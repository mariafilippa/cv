from django import forms
from django.contrib.admin import widgets

from gatekeeper.models import (
    Plan,
)


class PlanForm(forms.ModelForm):
    # field_order = []

    class Meta:
        model = Plan
        fields = '__all__'
        exclude = [
            'user',
        ]
