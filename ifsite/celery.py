from __future__ import absolute_import, unicode_literals
import os
import django
from django.utils import timezone
from django.template.loader import get_template
from django.core.mail import send_mail
from celery import Celery


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ifsite.settings')

django.setup()
from ifsite import settings
from ifsite.models import Profile
from gatekeeper.models import Plan

app = Celery('ifsite')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


@app.task(bind=True)
def mail_consolidated_at_sunset(self):
    today = timezone.localdate()
    new_plans = Plan.objects.filter(create_at__gte=today)
    new_num = new_plans.count()

    if new_num:
        subject = '[想像朋友] {0}新增{1}篇創作計畫'.format(today.strftime("%m/%d"), new_num)
        html_message = get_template('email/consolidated-notification.html').render({
            'new_plans': new_plans,
            'SITE_URL': settings.SITE_URL,
        })
        maillist = list(Profile.objects.new_plan_mail_targets(13).get_email())
        maillist = [settings.EMAIL_HOST_USER] if settings.DEBUG else maillist # 測試的時候寄給自己就好
        return send_mail(subject,
                         '',
                         settings.EMAIL_HOST_USER,
                         maillist,
                         html_message=html_message,
                         fail_silently=False)
