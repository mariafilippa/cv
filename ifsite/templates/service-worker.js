var cacheName = 'if-v0.0.1';
var getTitle = function (title) {
        if (title === "") {
                title = "TITLE DEFAULT";
        }
        return title;
};
var getNotificationOptions = function (body, tag, data) {
        var options = {
                body: body,
                icon: 'https://blog.bitbucket.org/wp-content/themes/bitbucket2012/images/bitbucket.png',
                tag: tag,
                data: data,
                vibrate: [200, 100, 200, 100, 200, 100, 200]
        };
        return options;
};

self.addEventListener('install', function (event) {
        self.skipWaiting();
        event.waitUntil(
          caches.open(cacheName).then(function(cache) {
            return cache.addAll(
              [
                '/static/css/sb-admin.min.css',
                '/static/js/sb-admin.min.js',
                '/static/vendor/bootstrap/css/bootstrap.min.css',
                '/static/vendor/bootstrap/js/bootstrap.bundle.min.js',
                '/static/vendor/font-awesome/css/font-awesome.min.css',
                '/static/vendor/jquery/jquery.min.js',
                '/static/if.png',
                '/static/manifest.json',
                'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css'
              ]
            );
          })
        );
});

self.addEventListener('activate', event => {
  event.waitUntil(clients.claim());
});

self.addEventListener('push', function(event) {
        try {
                // Push is a JSON
                var response_json = event.data.json();
                var title = response_json.title;
                var body = response_json.body;
                var tag = response_json.tag;
                var data = response_json.url;
        } catch (err) {
                // Push is a simple text
                var title = "";
                var body = event.data.text();
                var tag = "";
        }
        self.registration.showNotification(getTitle(title), getNotificationOptions(body, tag, data));
        // Optional: Comunicating with our js application. Send a signal
        self.clients.matchAll({includeUncontrolled: true, type: 'window'}).then(function (clients) {
                clients.forEach(function (client) {
                        client.postMessage({
                                "data": tag,
                                "data_title": title,
                                "data_body": body});
                        });
        });
});

// Optional: Added to that the browser opens when you click on the notification push web.
self.addEventListener('notificationclick', function(event) {
        // Android doesn't close the notification when you click it
        // See http://crbug.com/463146
        event.notification.close();
        // Check if there's already a tab open with this URL.
        // If yes: focus on the tab.
        // If no: open a tab with the URL.
        event.waitUntil(clients.matchAll({type: 'window', includeUncontrolled: true}).then(function(windowClients) {
                        var url = event.notification.data;
                        if (url === null) {url = '/plan/'};
                        for (var i = 0; i < windowClients.length; i++) {
                                var client = windowClients[i];
                                if ('focus' in client) {
                                        client.navigate(url);
                                        return client.focus();
                                }
                        }
                        return clients.openWindow(url);
                })
        );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});
