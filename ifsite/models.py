from django.db import models
from django.db.models import Q, Count
from django.urls import reverse


class ProfileQuerySet(models.QuerySet):
    def gatekeepers(self):
        return self.filter(is_gatekeeper=True)

    def new_plan_mail_targets(self, after_grade):
        return self.filter(
            Q(is_gatekeeper=True) | Q(grade__gte=after_grade)
        ).filter(subscribe_plan_mail=True)

    def get_email(self):
        return self.values_list('user__email', flat=True)

    def count_stump(self):
        return self.annotate(
            stump_num=Count(
                'user__keeper_krs',
                filter=Q(user__keeper_krs__status='ok'),
                distinct=True,
            ))


class ProfileManager(models.Manager):
    def get_queryset(self):
        return ProfileQuerySet(self.model, using=self._db).select_related('user')

    def gatekeepers(self):
        return self.get_queryset().gatekeepers()

    def new_plan_mail_targets(self, after_grade):
        return self.get_queryset().new_plan_mail_targets(after_grade)

    def get_email(self):
        return self.get_queryset().get_email()


class Profile(models.Model):
    objects = ProfileManager()

    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)
    grade = models.PositiveIntegerField('屆數', help_text='搶救文壇新秀再作戰的屆數')
    facebook = models.URLField('facebook', max_length=256, null=True, blank=True, help_text='臉書連結')
    penname = models.CharField('暱稱', max_length=16, null=True, blank=True, help_text='希望在會內被大家這樣稱呼，沒有的話就放心留空')
    birthday = models.DateField('生日', null=True, blank=True)
    phone1 = models.CharField(max_length=16, null=True, blank=True)
    phone2 = models.CharField(max_length=16, null=True, blank=True)
    skill = models.CharField('專長', null=True, blank=True, max_length=128)
    intro = models.TextField('自介', null=True, blank=True, help_text='')
    comment = models.CharField('備註', null=True, blank=True, max_length=128)
    area = models.CharField('地區', null=True, blank=True, max_length=32)
    is_gatekeeper = models.BooleanField('擔任守門人', default=False)
    subscribe_plan_mail = models.BooleanField('願意收到守門人計畫相關信件', default=False)
    subscribe_comments_mail = models.BooleanField('願意收到留言通知信件', default=True)

    @property
    def displayname(self):
        user = self.user
        if self.penname:
            return self.penname
        if len(user.first_name) == 1:
            return user.last_name + user.first_name
        return user.first_name

    def can_be_a_keeper(self):
        return self.is_gatekeeper and self.intro

    def get_stumps(self):
        return self.user.keeper_krs.filter(status='ok')

    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'Profiles'

    def get_absolute_url(self):
        return reverse('profile-detail', kwargs={'pk': self.pk})

    def get_previous_obj(self):
        return Profile.objects.filter(pk__lt=self.pk).order_by('pk').last()

    def get_next_obj(self):
        return Profile.objects.filter(pk__gt=self.pk).order_by('pk').first()

    def __str__(self):
        return '{0.grade}屆 - {0.displayname} 個人資料'.format(self)
