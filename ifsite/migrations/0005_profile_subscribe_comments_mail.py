# Generated by Django 2.0.3 on 2018-04-11 07:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ifsite', '0004_profile_subscribe_plan_mail'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='subscribe_comments_mail',
            field=models.BooleanField(default=True, verbose_name='願意收到留言通知信件'),
        ),
    ]
