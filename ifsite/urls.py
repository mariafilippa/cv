"""ifsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from gatekeeper.views import(
    ProfileView,
    PlanCreateView,
    PlanUpdateView,
    PlanListView,
    PlanDetailView,
    PlanUnsubscribeEmailView,
    ProfileDetailView,
    GatekeeperListView,
    GatekeeperDetailView,
    KeeperRequestListView,
    KeeperRequestDetailView,
    AcceptKeeperRequestView,
    RefuseKeeperRequestView,
    CancelKeeperRequestView,
    StumpCreateKeeperRequestView,
    KeeperCreateKeeperRequestView,
    CreatePlanCommentView,
    CreateGatekeeperCommentView,
    DeletePlanCommentView,
    DeleteGatekeeperCommentView,
    TerminateKeeperRequestView,
    ServiceWorkerView,
    SubscribeNotificationsView,
)
from django.views.generic import TemplateView, RedirectView

urlpatterns = [
    path('', RedirectView.as_view(pattern_name='plan-list', permanent=False), name='home'),


    path('accounts/profile/', ProfileView.as_view(), name='profile'),
    path('profile/<int:pk>/', ProfileDetailView.as_view(), name='profile-detail'),
    path('plan/create/', PlanCreateView.as_view(), name='plan-create'),
    path('plan/<int:pk>/update/', PlanUpdateView.as_view(), name='plan-update'),
    path('plan/<int:pk>/stump_request/', StumpCreateKeeperRequestView.as_view(), name='stump_request-create'),
    path('plan/<int:pk>/keeper_request/', KeeperCreateKeeperRequestView.as_view(), name='keeper_request-create'),
    path('plan/<int:pk>/comment/', CreatePlanCommentView.as_view(), name='plan_comment-create'),
    path('plan/<int:post_pk>/comment/<int:pk>/delete/', DeletePlanCommentView.as_view(), name='plan_comment-delete'),
    path('plan/', PlanListView.as_view(), name='plan-list'),
    path('plan/<int:pk>/', PlanDetailView.as_view(), name='plan-detail'),
    path('plan/unsubscribe_email/', PlanUnsubscribeEmailView.as_view(), name='plan-unsubscribe_email'),
    path('gatekeeper/', GatekeeperListView.as_view(), name='gatekeeper-list'),
    path('gatekeeper/<int:pk>/', GatekeeperDetailView.as_view(), name='gatekeeper-detail'),
    path('gatekeeper/<int:pk>/comment/', CreateGatekeeperCommentView.as_view(), name='gatekeeper_comment-create'),
    path('gatekeeper/<int:post_pk>/comment/<int:pk>/delete/', DeleteGatekeeperCommentView.as_view(), name='gatekeeper_comment-delete'),
    path('keeper_request/', KeeperRequestListView.as_view(), name='keeper_request-list'),
    path('keeper_request/<int:pk>/', KeeperRequestDetailView.as_view(), name='keeper_request-detail'),
    path('keeper_request/<int:pk>/accept/', AcceptKeeperRequestView.as_view(), name='keeper_request-accept'),
    path('keeper_request/<int:pk>/refuse/', RefuseKeeperRequestView.as_view(), name='keeper_request-refuse'),
    path('keeper_request/<int:pk>/cancel/', CancelKeeperRequestView.as_view(), name='keeper_request-cancel'),
    path('keeper_request/<int:pk>/terminate/', TerminateKeeperRequestView.as_view(), name='keeper_request-terminate'),

    path('admin/', admin.site.urls),
    path('accounts/register/', RedirectView.as_view(pattern_name='plan-list', permanent=False)),  # user register by themself is not allowed.
    path('accounts/', include('registration.backends.default.urls')),
    path('', include('django.contrib.auth.urls')),  # provid 'password_reset_confirm'
    # path('accounts/', include('registration.backends.simple.urls')),
    path('tinymce/', include('tinymce.urls')),
    
    path('service-worker.js', ServiceWorkerView.as_view()),
    path('notifications/subscribe/', SubscribeNotificationsView.as_view(), name='notifications-subscribe'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
