from django import forms
from django.contrib.admin import widgets

from ifsite.models import (
    Profile,
)


class ProfileForm(forms.ModelForm):
    # field_order = []

    class Meta:
        model = Profile
        fields = '__all__'
        exclude = [
            'user',
        ]
