﻿# coding=utf-8
import time
import random
import sys
import re
import logging
import os
import configparser
from datetime import datetime, date, timedelta

# import pickle
# from celery.utils.log import get_task_logger
# from redis import StrictRedis
# from redis.connection import BlockingConnectionPool
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import UnexpectedAlertPresentException
from PIL import Image
import hashlib
from Crypto.Cipher import AES
from binascii import b2a_hex, a2b_hex
import win32gui, win32com.client

# from report_web.settings import BASE_DIR
from .damatuWeb import *    # import的來源是在同一層, 否則會以為是從rest那層傳入
from .general_utility import ErrorTrackElasticsearch     # change one in initialize_pool

# # logging.basicConfig(filename="./cron/logger/bank_recon.log", level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# logHandler = TimedRotatingFileHandler("./cron/logger/bank_recon.log", when="w0", backupCount=3)
# logFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# logHandler.setFormatter(logFormatter)
#
# logger = logging.getLogger(__name__)
# # print(logger)
# logger.setLevel(logging.INFO)
# logger.addHandler(logHandler)

# logger = logging.getLogger(__name__)
# logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(name)s - %(levelname)s - %(message)s')

# redis_pool = StrictRedis(connection_pool=BlockingConnectionPool(max_connections=20))
# live_q = Queue(maxsize=15)
# off_q = Queue(maxsize=15)

config = configparser.ConfigParser()
config.read("./config/config.ini")
es_host = config['elasticsearch']

ES_HOSTNAME = es_host['host']
errTrack = ErrorTrackElasticsearch(index_title="brs", hostname=ES_HOSTNAME, user="ftp", passwd='t1elkftp2017')  # Bank_Reconciliation_System

dom_mapping = {
    "id": By.ID,
    "class": By.CLASS_NAME,
    "xpath": By.XPATH,
    "text": By.LINK_TEXT,
    "name": By.NAME
}

# desired_capabilities = {'browserName': 'internet explorer', 'ie.usePerProcessProxy': True, 'ie.setProxyByServer': True, 'silent': True}
desired_capabilities = {'browserName': 'internet explorer', 'proxy': {'httpProxy': '127.0.0.1:1080', 'noProxy': None, 'proxyType': 'MANUAL', 'class': 'org.openqa.selenium.Proxy', 'autodetect': False}, 'ie.usePerProcessProxy': True, 'ie.setProxyByServer': True, 'silent': True}
proxy_1080_q = []
proxy_1081_q = []

#每個視窗一個instance
class DriverWindow():

    def __init__(self, bank, bank_en, bank_account, merchant, account_id, account_password, beginDate):
        self.BANK = bank
        self.BANK_en = bank_en
        self.BANK_ACCOUNT = str(bank_account)
        self.MERCHANT = int(merchant)
        self.ACCOUNT_ID = str(account_id)
        self.balance = None
        self.begindate = beginDate
        # self.ENDDATE = datetime.now()
        self.status = 0
        # redis_pool.rpush('windows', pickle.dumps(self))
        merchant_key = '1052ed18deda78d500b513cfa98117eb'
        aes = AES.new(merchant_key[6:22], AES.MODE_CBC, merchant_key[6:22])
        self.ACCOUNT_PASSWORD = aes.decrypt(a2b_hex(account_password)).decode("utf-8").strip('\0')
        # self.ACCOUNT_PASSWORD = self.decrypt_pw(account_password, merchant_key_df, merchant)
        if self.BANK in proxy_1080_q:
            desired_capabilities['proxy']['httpProxy'] = '127.0.0.1:1081'
            proxy_1081_q.append(self.BANK)
            self.PROXY = proxy_1081_q
        else:
            proxy_1080_q.append(self.BANK)
            self.PROXY = proxy_1080_q       # self.PROXY is list of bank
        self.DRIVER = webdriver.Ie("./cron/IE/IEDriverServer.exe", desired_capabilities)
        self.DRIVER.set_page_load_timeout(15)

    def decrypt_pw(self, account_password, merchant_key_df, merchant):
        merchant_key = merchant_key_df[merchant_key_df["id"] == int(merchant)]["merchant_key"][0]
        aes = AES.new(merchant_key[6:22], AES.MODE_CBC, merchant_key[6:22])
        decryption_pw = aes.decrypt(a2b_hex(account_password)).decode("utf-8").strip('\0')
        return decryption_pw

    def endDate(self):
        return datetime.now()

    #前往Url
    def visit(self, url):
        self.DRIVER.get(url)

    #找到element輸入字串       input_by_id want to change to input_by_attribute
    def input_by_id(self, element_id, input_string, wait_sec=10, delay=0, dom="id"):
        element = WebDriverWait(self.DRIVER, wait_sec).until(EC.visibility_of_element_located((dom_mapping[dom], element_id)))
        element.click()
        element.clear()
        element.send_keys(input_string)
        time.sleep(delay)

    #找到按鈕按下
    def click_button(self, element_id, wait_sec=10, dom="id"):
        element = WebDriverWait(self.DRIVER, wait_sec).until(EC.element_to_be_clickable((dom_mapping[dom], element_id)))
        element.click()

    #輸入非字串
    def send_actions(self, key, repeat):
        for num in range(repeat):
            ActionChains(self.DRIVER).send_keys(key).perform()

    #儲存captcha圖檔
    def save_captcha(self, captcha_id, x2, y2, x1=0, y1=0, dom="id"):
        self.DRIVER.save_screenshot("./cron/screenshot/{}.png".format(self.BANK_ACCOUNT))
        img = self.check_load_success(captcha_id, dom=dom)
        loc = img.location
        image = Image.open("./cron/screenshot/{}.png".format(self.BANK_ACCOUNT))
        captcha_img = image.crop((loc['x']+x1, loc['y']+y1, loc['x']+x2, loc['y']+y2))
        captcha_img.save("./cron/captcha/{}.png".format(self.BANK_ACCOUNT))

    #captcha和手機確認碼輸入
    def captcha_input(self, captcha_id, button_id, captcha_dom="id", button_dom="id", captcha_code=200):
        element = self.check_load_success(captcha_id, captcha_dom)
        element.click()
        print(dmt.getBalance())
        if int(dmt.getBalance()) < 1000:
            errTrack.put_err_to_elasticsearch(fileName=str(__name__), msg="damatu balance < 2000")
        captcha = dmt.decode('./cron/captcha/{}.png'.format(self.BANK_ACCOUNT), captcha_code)
        # captcha = input(self.BANK)
        element.clear()
        element.send_keys(captcha.lower())
        time.sleep(1)
        self.click_button(button_id, dom=button_dom)

    #手機認證碼
    def phone_input(self, phone_id, phone_code, button_id, button_dom="id"):
        element = self.check_load_success(phone_id)
        element.click()

        # phone_code = input(self.BANK + " 手機認證碼: ")
        element.clear()
        element.send_keys(phone_code)
        time.sleep(1)
        self.click_button(button_id, dom=button_dom)

    #確認進入交易畫面
    def check_load_success(self, element_id, dom="id", wait_sec=15):
        return WebDriverWait(self.DRIVER, wait_sec).until(EC.visibility_of_element_located((dom_mapping[dom], element_id)))

    #關閉彈出alert
    def close_alert(self):
        WebDriverWait(self.DRIVER, 2).until(EC.alert_is_present())
        alert = self.DRIVER.switch_to.alert
        time.sleep(0.2)
        alert.accept()

    #關閉driver
    def close(self):
        self.DRIVER.close()

    #Debug
    def traceback(self, err):
        # now = time.strftime('%H:%M:%S', time.localtime(time.time()))
        traceback = sys.exc_info()[2]
        # print(self.BANK, err, 'exception in line', traceback.tb_lineno)
        return (self.BANK, err, 'exception in line', traceback.tb_lineno)

    # 交易日期
    def dailyQuery(self, beginDate, endDate):  # list 中儲存 datetime.datetime()的物件   **取出時需要strftime**
        dateList = []
        if type(beginDate) != str:
            if self.BANK in ["中国银行"]:
                dateList.append((endDate - timedelta(days=1)).strftime("%Y/%m/%d"))
                dateList.append(endDate.strftime("%Y/%m/%d"))
            elif self.BANK in ["中国光大银行", "中国民生银行", "渤海银行", "华夏银行"]:
                dateList.append((endDate - timedelta(days=1)).strftime("%Y-%m-%d"))
                dateList.append(endDate.strftime("%Y-%m-%d"))
            elif self.BANK in ["工商银行"]:
                dateList.append((endDate - timedelta(days=1)).strftime("%Y.%m.%d"))
                dateList.append(endDate.strftime("%Y.%m.%d"))
            else:
                dateList.append((endDate - timedelta(days=1)).strftime("%Y%m%d"))
                dateList.append(endDate.strftime("%Y%m%d"))
        else:
            beginDate = datetime.strptime(beginDate, "%Y%m%d")
            for daily in self.date_range(datetime(beginDate.year, beginDate.month, beginDate.day),
                                         datetime(endDate.year, endDate.month, endDate.day), timedelta(days=1)):
                if self.BANK in ["中国银行"]:
                    dateList.append(daily.strftime("%Y/%m/%d"))
                elif self.BANK in ["中国光大银行", "中国民生银行", "渤海银行", "华夏银行"]:
                    dateList.append(daily.strftime("%Y-%m-%d"))
                elif self.BANK in ["工商银行"]:
                    dateList.append(daily.strftime("%Y.%m.%d"))
                else:
                    dateList.append(daily.strftime("%Y%m%d"))
        return dateList

    def date_range(self, start, stop, step):
        while start < stop:
            yield start
            start += step


def windowEnumerationHandler(hwnd, top_windows):
    top_windows.append((hwnd, win32gui.GetWindowText(hwnd)))

#將window帶進focus
def rise(window, delay=0.5):
    time.sleep(delay)
    results = []
    top_windows = []
    win32gui.EnumWindows(windowEnumerationHandler, top_windows)
    for i in top_windows:
        if window in i[1].lower():
            #print(i)
            shell = win32com.client.Dispatch("WScript.Shell")
            shell.SendKeys('%')
            win32gui.ShowWindow(i[0],5)
            win32gui.SetForegroundWindow(i[0])
            break
    time.sleep(delay)

def CreateIdentityCode():
    randomIdentityCodeRange = range(1000, 9999)
    randomIdentityCode = random.sample(randomIdentityCodeRange, 1)[0]  # return list 取index
    return randomIdentityCode

def clean_text(text):
    if text:
        return text.strip()
    else:
        return text
