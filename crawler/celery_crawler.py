from celery import Celery
from celery.schedules import crontab
from celery.result import ResultSet
import pandas as pd
from gevent.queue import Queue as gQueue, Empty
import os
import configparser

# from report_web.settings import BASE_DIR
from .login_all import *
from .crawl_all import *
from .transfer_all import *

mysql_pool.close()

config = configparser.ConfigParser()
config.read("./config/config.ini")
redis_server = config['redis']

app = Celery('celery_test', backend='redis://{}:{}'.format(redis_server['host'], redis_server['port']), broker='redis://{}:{}'.format(redis_server['host'], redis_server['port']))  # Celery('celeryName', broker='redis://IP')
app.config_from_object('cron.celeryconfig')              # 讀取config
# app.conf.beat_schedule = {
#     'run-every-weekday': {
#         'task': 'celery_test.test',
#         'schedule': crontab(minute='*/ '),
#     },
# }

live_q = gQueue(maxsize=15)
login_q = gQueue(maxsize=15)
crawl_q = gQueue(maxsize=15)


#新增視窗
def initialize_window(*args, web=False):
    window = DriverWindow(*args)
    return login_gateway(window, web=web)


#主登入程序
def login_gateway(window, web=False):

    login = {

    }

    for _ in range(2):
        try:
            window = login[window.BANK](window)
            # logger.info(window.BANK + " done login")
            # errTrack.put_info_to_elasticsearch(bankName=window.BANK, fileName=str(__name__), msg="done login")
            window.status = 1
            if web:
                live_q.put(window)
                return window.BANK_ACCOUNT, datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            else:
                return crawler_gateway(window)
        except Exception as err:
            # window.traceback(err)
            # logger.warning(traceback(err))
            errTrack.put_warning_to_elasticsearch(merchant=window.MERCHANT, bankName=window.BANK, fileName=str(__name__), errType=str(type(err).__name__), err=err)
            continue
    window.PROXY.remove(window.BANK)
    window.close()
    if web:
        return window.BANK_ACCOUNT, 0
    else:
        # logger.error(window.BANK + " login failed 3 times! Please check the site immediately!")
        errTrack.put_err_to_elasticsearch(merchant=window.MERCHANT, bankName=window.BANK, fileName=str(__name__), msg="login failed 2 times! Please check the site immediately!")
        return window.BANK, window.BANK_ACCOUNT, window.status


#主爬蟲程序
def crawler_gateway(window):

    crawl = {

    }

    for _ in range(5):
        try:
            # if window:
            # logger.info(window.BANK + "start crawling")
            # errTrack.put_info_to_elasticsearch(bankName=window.BANK, fileName=str(__name__), msg="start crawler")
            window = crawl[window.BANK](window)
            # logger.info(window.BANK + " done crawling")
            # errTrack.put_info_to_elasticsearch(bankName=window.BANK, fileName=str(__name__), msg="done crawling")
            live_q.put(window)
            return window.BANK, window.BANK_ACCOUNT, datetime.now().strftime('%Y-%m-%d %H:%M:%S'), window.balance
            # else:
            #     return
        except Exception as err:
            # window.traceback(err)
            # logger.warning(traceback(err))
            errTrack.put_warning_to_elasticsearch(merchant=window.MERCHANT, bankName=window.BANK, fileName=str(__name__), errType=str(type(err).__name__), err=err)
            continue

    # logger.error(window.BANK + " crawler failed 5 times! Please check the site immediately!")
    # errTrack.put_err_to_elasticsearch(merchant=window.MERCHANT, bankName=window.BANK, fileName=str(__name__), msg="crawler failed 5 times! Please check the site immediately!")
    window.status = 0
    live_q.put(window)
    return window.BANK, window.BANK_ACCOUNT, 0


#新增視窗
@app.task
def initialize(*args, web=False):
    return initialize_window(*args, web=web)


#主登入程序
@app.task
def login():
    window = login_q.get()
    return login_gateway(window)


#主爬蟲程序
@app.task
def crawl():
    window = crawl_q.get()
    return crawler_gateway(window)


@app.task
def cron_update(accounts_csv):
    # accounts_csv = pd.read_csv("./1_accounts.txt", header=None)
    print(accounts_csv)
    merchant = accounts_csv['merchant_id'][0]
    config = configparser.ConfigParser()
    config.read("./cron/beginDate.ini", encoding="utf-8-sig")  # (config return dict)
    beginDate_info = config['bank.beginDate']
    last_update_info = config['bank.last_update']
    # with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    #     account_futures = {executor.submit(login_gateway, account[0], account[1], account[2]): account for account in accounts}
    # concurrent.futures.wait(account_futures)

    jobs = ResultSet([])
    accounts_missing = accounts_csv[accounts_csv['active']==str(1)]   # 第一次先取出需要爬取的銀行，再去和Queue比對後沒有再Queue的丟到except後break重來一次
    while True:
        try:
            window = live_q.get_nowait()                                        # 讀取到在celery的queue
            account = window.BANK_ACCOUNT
            if account in accounts_missing['bank_account'].tolist():                            # window關閉機制
                accounts_missing = accounts_missing[accounts_missing['bank_account']!=account]  # 有在需要爬取名單  但不在queue中
                status = window.status
                if status == 0:
                    login_q.put(window)
                    jobs.add(login.delay())
                if status == 1:
                    if beginDate_info[window.BANK]:
                        window.begindate = beginDate_info[window.BANK]
                    else:
                        window.begindate = datetime.now()
                    crawl_q.put(window)
                    jobs.add(crawl.delay())
            else:
                window.PROXY.remove(window.BANK)
                window.close()
        except Empty:
            for row in range(len(accounts_missing['active'])):                         # accounts_missing第0欄的筆數
                if beginDate_info[accounts_missing.iloc[row]['bank_id']]:
                    beginDate = beginDate_info[accounts_missing.iloc[row]['bank_id']]
                else :
                    beginDate = datetime.now()
                jobs.add(initialize.delay(accounts_missing.iloc[row]['bank_id'], accounts_missing.iloc[row]['bank_id_en'], accounts_missing.iloc[row]['bank_account'], accounts_missing.iloc[row]['merchant_id'], str(accounts_missing.iloc[row]['account_id']), str(accounts_missing.iloc[row]['account_pw']), beginDate))
            break     # 跳出while True 進入gevent.wait()等待工作結束  同時等待celery下次排成的執行 此時的queue是存在celery當中

    results = jobs.get()  # ResultSet is append task and use get() is return tuple of set.
    # logger.info("cycle over. waking up in 5 min")
    logmsg = {}
    for result in results:
        if result:
            logmsg[result[0]] = result[2]
            # logmsg.__setitem__(jobs[idx].value[0], jobs[idx].value[1])
            if type(result[2]) == str and result[2] != str(0):
                config.set("bank.beginDate", result[0], "")
                config.set('bank.last_update', result[0], result[2])
            else:
                if not last_update_info[result[0]] or (datetime.strptime(last_update_info[result[0]], '%Y-%m-%d %H:%M:%S') + timedelta(minutes=15)) < datetime.now():
                    errTrack.put_err_to_elasticsearch(merchant=merchant, bankName=result[0], fileName=str(__name__), msg="crawler is down for 15 mins! Please check the site immediately!")
    try:
        config.write(open("./cron/beginDate.ini", "w", encoding="utf-8-sig"))
    except:
        pass
    print("cycle over. waking up in 5 min")
    errTrack.put_info_to_elasticsearch(fileName=str(__name__), msg="cycle over. waking up in 5 min", monitor=str(logmsg))
    return results
