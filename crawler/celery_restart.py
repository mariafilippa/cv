import os
import time
import win32gui, win32con, win32api
import os
import configparser

from celery import Celery
from celery.schedules import crontab
from report_web.settings import BASE_DIR
from .sql_functions import insertMerchantKey, mysql_pool, errTrack

config = configparser.ConfigParser()
config.read(os.path.join(BASE_DIR, "./config/config.ini"))
redis_server = config['redis']

app = Celery('celery_test', backend='redis://{}:{}'.format(redis_server['host'], redis_server['port']), broker='redis://{}:{}'.format(redis_server['host'], redis_server['port']))  # Celery('celeryName', broker='redis://IP')
app.config_from_object('cron.celeryconfig')              # 讀取config

mysql_pool.close()

def windowEnumerationHandler(hwnd, top_windows):
    top_windows.append((hwnd, win32gui.GetWindowText(hwnd)))

def close_window(window, delay=0.5):
    time.sleep(delay)
    results = []
    top_windows = []
    win32gui.EnumWindows(windowEnumerationHandler, top_windows)
    print(top_windows)
    for i in top_windows:
        if window in i[1].lower():
            win32gui.PostMessage(i[0],win32con.WM_CLOSE,0,0)

@app.task
def restart():
    errTrack.put_err_to_elasticsearch(fileName=str(__name__), msg="celery worker down for 5 min. restarting...", bankName="")
    close_window('new_worker.bat')
    close_window('internet explorer')
    time.sleep(10)
    win32api.WinExec('cmd /c start cmd /k "{}/new_worker.bat"'.format(os.path.dirname(__file__)))
        # result = timer.delay(accounts_csv)
        # try:
        #     return result.get()
        # except TimeLimitExceeded:

@app.task
def ping():
    print('nothing. just checking.')

@app.task
def insertKeys(merchant_key_dict):
    insertMerchantKey(merchant_key_dict)
