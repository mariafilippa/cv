from queue import Queue
import os
import re
import configparser
from datetime import datetime, timedelta

import pymysql
import pymysql.cursors
from pymysql import err

# from report_web.settings import BASE_DIR
from .general_utility import ErrorTrackElasticsearch

config = configparser.ConfigParser()
config.read("./config/config.ini")
mysql_db = config['mysql.server']
es_host = config['elasticsearch']

ES_HOSTNAME = es_host['host']
errTrack = ErrorTrackElasticsearch(index_title="brs", hostname=ES_HOSTNAME, user="ftp", passwd='t1elkftp2017')  # Bank_Reconciliation_System


null_placeholder = ["", "--", "\xa0"]

currency = {
    '1': ['人民币元', '人民币', 'RMB', 'CNY']
}


#MySQL connection pool
class ConnectionPool():

    def __init__(self, max_pool_size=20):
        self.max_pool_size = max_pool_size
        self.initialize_pool()

    @staticmethod
    def connection():
        conn = pymysql.connect(host=mysql_db['host'],
                                user=mysql_db['user'],
                                password=mysql_db['password'],
                                database='bank_recon',
                                charset='utf8mb4',
                                cursorclass=pymysql.cursors.DictCursor,
                                autocommit=True)
        return conn

    #建立connection pool
    def initialize_pool(self):
        self.pool = Queue(maxsize=self.max_pool_size)
        for _ in range(0, self.max_pool_size):
            try:
                # connection = pymysql.connect(host=mysql_db['host'],
                #                              user=mysql_db['user'],
                #                              password=mysql_db['password'],
                #                              database='bank_recon',
                #                              charset='utf8mb4',
                #                              cursorclass=pymysql.cursors.DictCursor,
                #                              autocommit=True)
                self.pool.put(ConnectionPool.connection())
            except err.MySQLError as error:
                # logger.error(error)
                errTrack.put_err_to_elasticsearch(bankName="initialize_pool", fileName=str(__name__), errType=str(type(err).__name__), msg=error.__str__())


    #自pool請求一條連線
    def get_connection(self):
        # returns a db instance when one is available else waits until one is
        connection = self.pool.get(True)
        # checks if db is still connected because db instance automatically closes when not in used
        if not self.ping(connection):
            connection.connect()
        return connection

    #歸還連練
    def return_connection(self, connection):
        return self.pool.put(connection)

    #確認連線存活
    def ping(self, connection):
        try:
            return connection.query('SELECT 1', [])
        except:
            return

    #查詢是否有可用連線
    def is_empty(self):
        return self.pool.empty()

    #close pool
    def close(self):
        while not self.is_empty():
            self.pool.get().close()


mysql_pool = ConnectionPool()


#將一筆交易紀錄存入MySQL
def mysql_insert(bank_transaction):
    bank_transaction['merchant'] = int(bank_transaction['merchant'])
    bank_transaction['bank_account'] = str(bank_transaction['bank_account'])
    if 'identity_code' not in bank_transaction:
        bank_transaction['identity_code'] = None

    # for currency_id in currency:
    #     if bank_transaction['currency'] in currency[currency_id]:
    #         bank_transaction['currency'] = currency_id
    #         break
    for currency_id in currency:
        for idx in range(len(currency[currency_id])):
            if currency[currency_id][idx] in bank_transaction['currency']:
                bank_transaction['currency'] = currency_id
                break

    #檢查Schema
    if 'deposit' in bank_transaction:
        if bank_transaction['deposit'] in null_placeholder:
            bank_transaction['deposit'] = None
        if bank_transaction['withdrawal'] in null_placeholder:
            bank_transaction['withdrawal'] = None

        if bank_transaction['deposit']:
            if bank_transaction['withdrawal']:
                # logger.warning(bank_transaction['bank'] + " Schema might have changed!")
                errTrack.put_err_to_elasticsearch(merchant=bank_transaction['merchant'], bankName=bank_transaction['bank'], fileName=str(__name__),  msg="Schema might have changed!")
            else:
                bank_transaction['status'] = 1
                bank_transaction['amount'] = bank_transaction['deposit']
        elif bank_transaction['withdrawal']:
            bank_transaction['status'] = 2
            bank_transaction['amount'] = bank_transaction['withdrawal']
        else:
            # logger.warning(bank_transaction['bank'] + " Schema might have changed!")
            errTrack.put_err_to_elasticsearch(merchant=bank_transaction['merchant'], bankName=bank_transaction['bank'], fileName=str(__name__), msg="Schema might have changed! - deposit")

    if re.compile('^[.0-9]+$').match(bank_transaction['amount']) == None:
        # logger.warning(bank_transaction['bank'] + " Schema might have changed!")
        errTrack.put_err_to_elasticsearch(merchant=bank_transaction['merchant'], bankName=bank_transaction['bank'], fileName=str(__name__), msg="Schema might have changed! - amount")

    if re.compile('^[.0-9]+$').match(bank_transaction['balance']) == None:
        # logger.warning(bank_transaction['bank'] + " Schema might have changed!")
        errTrack.put_err_to_elasticsearch(merchant=bank_transaction['merchant'], bankName=bank_transaction['bank'], fileName=str(__name__), msg="Schema might have changed! - balance")

    depositList = ['汇款', '网上银行', '转账', '汇款汇入', '他行来账', '跨行汇款', '网银互联汇入', '跨行转账', '网银互联', '网络银行', '跨行支付', '网银跨行汇款', '网银入账',
                '跨行转账 / 转账']
    for depositStr in depositList:
        if depositStr in bank_transaction['type'] or re.compile('[.0-9]+').search(bank_transaction['type']):
            bank_transaction['type'] = "transfer"
            break

    print(bank_transaction)

    connection = mysql_pool.get_connection()
    try:
        with connection.cursor() as cursor:
            sql = "INSERT IGNORE INTO `bank_transaction` (`id`, `type`, `status`, `transaction_time`, `amount`, `balance`, `currency_id`, `identity_code`, `bank_account_id`, `bank_id`, `bank_id_en`, `merchant_id`, `created_at`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # if cursor.execute(sql, (bank_transaction['id'], bank_transaction['type'], bank_transaction['status'], bank_transaction['transaction_time'], bank_transaction['amount'], bank_transaction['balance'], bank_transaction['currency'], bank_transaction['identity_code'], bank_transaction['bank_account'], bank_transaction['bank'], bank_transaction['merchant'], datetime.now())):
            #檢查入賬紀錄是否含交易識別碼
            # if bank_transaction['identity_code'] == None and bank_transaction['status'] == 1:
            #     query = "SELECT LAST_INSERT_ID() from `bank_transaction`"    # 自動assign id (auto_increment)
            #     _id = cursor.execute(query)
            #     logger.warning("id: {} 入賬紀錄沒有識別碼!".format(bank_transaction['id']))
            cursor.execute(sql, (bank_transaction['id'], bank_transaction['type'], bank_transaction['status'],
                                 bank_transaction['transaction_time'], bank_transaction['amount'],
                                 bank_transaction['balance'], bank_transaction['currency'],
                                 bank_transaction['identity_code'], bank_transaction['bank_account'],
                                 bank_transaction['bank'], bank_transaction['bank_en'], bank_transaction['merchant'], datetime.now()+timedelta(hours=8)))
    except Exception as err:
        print(err)
    mysql_pool.return_connection(connection)

# def traceback(err):
#     # now = time.strftime('%H:%M:%S', time.localtime(time.time()))
#     traceback = sys.exc_info()[2]
#     print(err, 'exception in line', traceback.tb_lineno)
#     return (err, 'exception in line', traceback.tb_lineno)

def insertMerchantKey(merchant_key_dict):
    conn = ConnectionPool.connection()
    if not conn:
        errTrack.put_err_to_elasticsearch(merchant=merchant_key_dict['id'], bankName="merchantKey_insert_conn_initialize",
                                          fileName=str(__name__), msg="sql_function conn err")
    sql = """select merchant_key from `keys` where id = {}""".format(merchant_key_dict['id'])
    insert = """INSERT IGNORE INTO `keys` (`id`, `merchant_key`) VALUES (%s, %s)"""
    update = """update `keys` set `merchant_key`="{}" where `id`={}""".format(merchant_key_dict['merchant_key'], merchant_key_dict['id'])
    with conn.cursor() as cursor:
        merchant_key = cursor.execute(sql)
        if not merchant_key:
            cursor.execute(insert, (merchant_key_dict['id'], merchant_key_dict['merchant_key']))
        else:
            cursor.execute(update)
    conn.close()
